<?php

use Illuminate\Http\Request;

/*会員登録
Route::group(['middleware' => ['auth:api']],function(){
}

});
*/

Route::group(['prefix'=>'v1'],function(){
//Q&A
    Route::get('/questions', 'QuestionController@list'); //一覧データ
    Route::group(['prefix'=>'question'],function(){
        Route::get('/{id}/get', 'QuestionController@show'); //個別データ
        Route::get('/results', 'QuestionController@results'); //複数データ
        Route::post('/commit', 'QuestionController@store');
        Route::put('/{id}/update', 'QuestionController@update');
        Route::delete('/{id}/delete', 'QuestionController@delete');
        //Route::post('/answer/commit', 'QuestionController@store');//ログインしているユーザー
        //Route::put('/{id}/update', 'QuestionController@update');//ログインしているユーザーの回答を更新、メソッドはどうすればいい？
      });

//質問回答
    Route::group(['prefix'=>'answer'],function(){
        Route::get('/{id}/get', 'AnswerController@show'); //個別データ
        Route::post('{id}/commit', 'AnswerController@store');
      });

//開発キャンバス
    Route::group(['prefix'=>'canvas'], function(){
        Route::post('/commit', 'CanvasController@store');
        Route::put('/{user_id}/update', 'CanvasController@update');
        Route::get('/{user_id}/get', 'CanvasController@show');
        Route::delete('{user_id}/delete', 'CanvasController@delete');
        Route::post('/{user_id}/consultCommit', 'CanvasController@store');
        Route::put('/{user_id}/consultUpdate', 'CanvasController@update');
      });

//課題機能
    Route::get('/tasks', 'TaskController@list');
    Route::group(['prefix'=>'task'], function(){
        Route::get('/{id}/get', 'TaskController@show');
        Route::post('/commit', 'TaskController@store');
        Route::put('/{id}/update', 'TaskController@update');
        Route::delete('{id}/delete', 'TaskController@delete');
      });

    Route::get('/categories', 'CategoryController@list'); //一覧データ

     /*問い合わせ
         Route::post('/contact/commit', 'ContactController@store');
     */
      //chatbot
    Route::group(['prefix'=>'chat'],function(){
        Route::post('/','ChatController@reply');
       // Route::get('/','ChatController@get_categories');
        Route::post('/test','ChatController@check_existence');
        Route::post('/test2','ChatController@search_id');
        Route::post('/api','ChatController@google_api');
         Route::post('/api2','ChatController@google_api2');
    });
});

