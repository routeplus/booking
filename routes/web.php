<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/state', 'UserController@show'); //ユーザー情報GET




Route::get('/', function () {
    return view('top.index');
});

//もくもくSPA用
Route::group(['prefix' => 'mokumoku'], function(){
  Route::any('{any}',function () {
      return view('top.index');
  });
});


Route::get('/practice', function () {
    return view('practice.practice');
});

Route::get('/canvas', function () {
    return view('canvas.index');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/questions", 'QuestionController@index');
Route::group(['prefix'=>'question'],function(){
    Route::get("/", 'QuestionController@index');
    Route::get("/{id}", 'QuestionController@detail');
});

/*====== chatbot =====*/
Route::group(['prefix'=>'chat'],function(){
    Route::get('/','ChatController@index');
    Route::post('/','ChatController@reply');
});


/*====== mypage =====*/
Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix'=>'mypage'],function(){
        Route::get("/", 'MypageController@index');
        Route::post("/commit/{id}", 'MypageController@store');
        Route::get('/edit', 'MypageController@show');
        Route::post("/edit", 'MypageController@update');
//        Route::delete("/edit", 'MypageController@destroy');
    });
});

Route::get("/test","Admin_menuController@index");