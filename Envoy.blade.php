@servers(['stg-host' => 'deployer@'52.69.45.203, 'prd-host' => 'deployer@52.69.45.203'])

@setup
  $app_dir = '/var/www/kokokara/src'
@endsetup

@story('deploy_stg', ['on' => 'stg-host'])
  pull_repository
  run_composer
  run_artisan
  run_npm
@endstory

@story('deploy_prd', ['on' => 'prd-host'])
  pull_repository
  run_composer
  run_artisan
  run_npm
@endstory

@task('pull_repository')
  echo 'Pull repository'
  cd {{ $app_dir }}
  git pull
@endtask

@task('run_composer')
  cd {{ $app_dir }}
  composer update
  composer install --no-dev --optimize-autoloader
@endtask

@task('run_artisan')
  cd {{ $app_dir }}
  php artisan down
  php artisan migrate
  php artisan cache:clear
  php artisan config:cache
  php artisan view:clear
  php artisan up
@endtask

@task('run_npm')
  cd {{ $app_dir }}
  npm run production
@endtask
