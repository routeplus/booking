<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('プログラミング');
        });
    }
    public function testQuestions()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/questions')
                    ->assertSee('Q&A');
        });
    }
}
