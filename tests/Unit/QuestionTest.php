<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/questions');
        $this->assertTrue(isset($response));
    }

    public function testGetQuestions()
    {
        $response = $this->get('/question/1/get');
        $this->assertTrue(isset($response));
    }


}
