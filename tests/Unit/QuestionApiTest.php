<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testApiGetQuestion()
    {
        $response = $this->json('GET', '/api/v1/questions');
        $response
            ->assertStatus(200);
    }
}
