<?php

namespace App\Http\Controllers;

use App\Http\Requests\MypageRequest;
use Illuminate\{Http\Request, Support\Facades\Auth, Support\Facades\Storage};
use App\Http\Models\User;
use Session;

class MypageController extends MyController
{
    private $user;

    public function __construct()
    {
        //Laravel5.5ではコンストラクタでAuthが使えなくなったので、半ば無理やりｗ
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        $disk = Storage::disk('gcs');
        $this->user->image = $disk->url("images/users/1.png");
        return view('mypage.index')->with('user', $this->user);

    }

    public function store(Request $request, $id)
    {
        $this->validate($request, ['file' => 'required|image']);
        $image = $request->file('file');
        $ext = $image->getClientOriginalExtension();
        $filename = $id. '.' .$ext;

        $disk = Storage::disk('gcs');
        $disk->put("images/users/$filename", $image);

        return redirect('/mypage')->with('message', 'プロフィールを更新しました！');

    }

/*
    public function store(Request $request)
    {
        $post = $request->all();

        $post->save();

        return $post->title;
    }
*/

    public function show()
    {
        return view('mypage.edit')->with('user', $this->user);
    }

    public function update(MypageRequest $request)
    {

        $user = Auth::user();

        $user->name = $request->name;
        $user->email = $request->email;

        $user->save();

        if($request->has('password'))
        {
            $user->password = bcrypt($request->password);
            $user->save();
        }

/* WIP
      if($request->hasFile('image')){
            $file = $request->file('image');
            $name = time() . $file->getClientOriginalName();
            $filePath = 'images/' . $name;
            Storage::disk('s3')->post($filePath, file_get_contents($file));
        }
*/

        return back()->withSuccess('プロフィールが更新されました！');
    }

/* WIP. Make a button on the view.
       public function destroy($file)

    {
        Storage::disk('s3')->delete('images/' . $file);

        return back()->withSuccess('画像が削除されました！');
    }
*/

}
