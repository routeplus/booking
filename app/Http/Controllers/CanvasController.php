<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Canvas;

class CanvasController extends MyController
{
    public function __construct(Canvas $canvas)
    {
      $this->canvas = $canvas;
    }

    public function show($user_id)
    {
        $data = $this->canvas->findByUserId($user_id);
        return $data;
    }

    public function store(Request $request)
    {
        $data = $request->isMethod('put') ? Canvas::findOrFail($request->canvas_id) : new Canvas;
        $data->user_id = $request->input('user_id');
        $data->like = $request->input('like');
        $data->app = $request->input('app');
        $data->field = $request->input('field');
        $data->feature = $request->input('feature');
        $data->message = $request->input('message');

        $data->save();

        return $data->title;
    }

    public function update(Request $request, $user_id)
    {
        $data = Canvas::findByUserId($user_id);
        $data->like = $request->like;
        $data->app = $request->app;
        $data->field = $request->field;
        $data->feature = $request->feature;
        $data->message = $request->message;

        $data->save();

        return redirect('api/v1/canvas/{use_id}/update');
    }

    public function delete(Canvas $canvas, $user_id)
    {
        $data = Canvas::findOrFail($user_id);
        $data->delete();

        return 204;
    }
}
