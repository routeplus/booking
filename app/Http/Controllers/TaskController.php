<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Task;

class TaskController extends MyController
{

    public function __construct(Task $task)
    {
      $this->task = $task;
    }


    public function list()
    {
        $data = $this->task::all();
        return $data;
    }

    public function show($id)
    {
        $data = $this->task::find($id);
        return $data;
    }


    public function store(Request $request)
    {
        $post = $request->isMethod('put') ? Task::findOrFail($request->task_id) : new Task;
        $post->title = $request->input('title');
        $post->body = $request->input('body');

        $post->save();

        return $post->title;
    }

    public function update(Request $request, $id)
    {
        $post = Task::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();

        return \Response::json($post);
    }

    public function delete($id)
    {
        $data = Task::findOrFail($id);
        $data->delete();

        return 204;
    }
}
