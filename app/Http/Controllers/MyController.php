<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class MyController extends Controller
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // $fileはファイル名単品
    public function s3Upload($directory, $file)
    {
        $path = Storage::disk('s3')->putFile($directory, $file, 'public');

        return $path;
    }

}
