<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Question;

class QuestionController extends MyController
{

    public function __construct(Question $question)
    {
      $this->question = $question;
    }

    public function list(Request $request)
    {
        $data = $this->question->getLists($request);
        return $data;
    }

    public function index()
    {
        return view('question.index');
    }

    public function detail($id)
    {
        return view('question.detail');
    }

    public function show($id)
    {
        $data = $this->question->getDetail($id);
        return $data;
    }

    public function results(Request $request)
    {
        $get = $request->all();
        array_key_exists('where', $get) ? $where = json_decode($get['where'], TRUE) : $where = NULL;
        //return $where;
        unset($get['where']);
        $results = $this->question->results($where, $get);
        return $results;
    }

    public function store(Request $request)
    {
        $post = $request->isMethod('put') ? Question::findOrFail($request->question_id) : new Question;
        $post->user_id = $request->input('user_id');
        $post->mentor_id = $request->input('mentor_id');
        $post->title = $request->input('title');
        $post->body = $request->input('body');

        $post->save();

        return $post->title;
    }

    public function update(Request $request, Question $question, $id)
    {
        $post = Question::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();

        return redirect('api/v1/question/{id}/update');
    }

    public function delete(Question $question, $id)
    {
        $data = Question::findOrFail($id);
        $data->delete();

        return 204;
    }

}
