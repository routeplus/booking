<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Answer;

class AnswerController extends Controller
{

    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }

    public function show($id)
    {
        $data = $this->answer->findById($id);
        return $data;
    }

    public function store(Request $request,$id)
    {
        $post = $request->all();
        //return $post;

        $user = \Auth::user();
        //if(!$user=\Auth::user() ){ return false; }

        $val = ['question_id' => $id, 'body' => $post['body'], 'category_id' => $post['category_id'], 'user_id' =>  $user->id];

        $this->answer->insert($val);

        return $id;
    }
}
