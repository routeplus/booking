<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ChatController extends Controller
{
   public function index() 
   {
   		return view('chat.index');
   
	}
	//categoyの中を全部出力
	/*public function get_categories()
	{
		$item = \DB::table('categories')->get();
    	return $item;
	}*/
	//純数にpostされたものを返す
	public function reply(Request $request)
	{	
		$message = $request->all();
		return "サーバー側に送られています〜！！";
	}
	//postされたもののidを調べる→この段階であるか無いか調べられる
	public function search_id(Request $request){
		$category = DB::table('categories');

		$search_key = $request->input('chat');
		$key = $category->where('name',$search_key)->get();
		if(0 < count($key)){
			return '答えられます';
		}else{
			return '答えられません';
		}
	}

	//送信されてきたもののIDを特定する
	public function check_existence(Request $request)
	{	
		//$answer = DB::table('base_questions');
		$category = DB::table('categories');
		//jsonで送られてきたデータのidを取得
		$response_data = $request->all();
		//$search_key = response()->json($response_data);
		//あるか無いかの判断	
		$data = $category->find($response_data['id']);
		$response_category = $data->name;
		if($data == true){
			return $response_category."についてのご質問ですね！ご相談内容を教えてください！";
		}else{
			return 'false';
		}
	}

	//google natural language
	public function google_api(Request $request)
	{
		$text = "httpリクエストをもっと理解したい";
		$params = [
			'document'=>[
				'type'=>'PLAIN_TEXT',
			    'content'=>$text
			  ],
			  'features'=>[
			    'extractSyntax'=>true
			  ]
			];

		exec('curl "https://language.googleapis.com/v1beta1/documents:annotateText?key=AIzaSyCWsCvsF8rnmDRbZQQKzRKbB_ommBOOjEA" \
  			-s -X POST -H "Content-Type: application/json;charset=utf-8" -d' . "'". json_encode($params,JSON_UNESCAPED_UNICODE) . "'",$data);
		$results = $request->all();
		$tes = response()->json($results);
		 return $data;
	}
	//google natural language
	public function google_api2(Request $request)
	{
		$text = "httpリクエストをもっと理解したい";
		$params = [
			'document'=>[
				'type'=>'PLAIN_TEXT',
			    'content'=>$text
			  ],
			  'features'=>[
			    'extractSyntax'=>true
			  ]
			];
		$url =  "https://language.googleapis.com/v1beta1/documents:annotateText?key=AIzaSyCWsCvsF8rnmDRbZQQKzRKbB_ommBOOjEA";
		
		$header = [
				'COontent-type:Apllication/json',
		];

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST'); // post
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params)); // jsonデータを送信
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header); // リクエストにヘッダーを含める
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);

		$response = curl_exec($curl);

		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE); 
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		$result = json_decode($body, true); 

		curl_close($curl);

		return $result;
	}
}