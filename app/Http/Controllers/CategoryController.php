<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Category;

class CategoryController extends Controller
{
    protected $table = 'categories';

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function list(Request $request)
    {
        $get = $request->all();
        $results = $this->category->results($get, ['type_id' => $get['type']]);
        return response()->json($results);

    }
    //
}
