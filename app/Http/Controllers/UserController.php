<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use Auth;

class UserController extends MyController
{
    public function show()
    {
        return response()->json(Auth::user(), 200);
    }
}
