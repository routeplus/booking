<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Contact;

class ContactController extends MyController
{
  public function __construct(Contact $contact)
  {
   //最初にインスタンス化してイベントモデルを使えるようにする
    $this->contact = $contact;
  }

  public function store(Request $request)
  {
        $contact = new Contact;
        $contact->body = $request->body;
        return $contact->save();

        }
  }
