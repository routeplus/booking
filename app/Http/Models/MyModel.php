<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class MyModel extends Model
{

    public function findById($id) {
        return DB::table($this->table)->where('id',$id)->first();
    }

#user_idを探す
    public function findByUserId($user_id) {
        return DB::table($this->table)->where('user_id',$user_id)->get();
    }

    public function results($params = [], $where=NULL ) {
        $offset = (isset($params['offset'])) ? $params['offset'] : 0;
        $limit = (isset($params['limit'])) ? $params['limit'] : 30;
        $orderBy = (isset($params['orderBy'])) ? $params['orderBy'] : 'id,DESC';
        $order = explode(",",$orderBy);

        if(count($order) == 1) {
            $order[1] = "DESC";
        }

        $results = DB::table($this->table)
            ->offset($offset)
            ->limit($limit)
            ->orderBy($order[0],$order[1])
            ->when($where, function ($query) use ($where) {
                return $query->where($where);
            })
            ->get();
        return $results;
        /*
        return response()->json(
        [
            'data' => $results
        ],
        200,[],
        JSON_UNESCAPED_UNICODE
        );
         */
    }

}
