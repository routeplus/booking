<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Question extends MyModel
{
    protected $table = 'questions';

    public function getData($id=null)
    {
        $quesry = DB::table($this->question);

        if($id != null) $query->where('id', $id);

        $data = $query->get();
        
        return $data;
        //return $this->hasOne('App\Http\Models\Question', 'user_id');
    }

    public function getLists($request)
    {
      return DB::table('questions AS q')->select('q.*', 'c.name AS category_name')
            ->join('categories AS c', 'c.id', '=', 'q.category_id')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function getDetail($id)
    {
      return DB::table('questions AS q')->select('q.*', 'a.id AS answer_id', 'a.body AS answer_body', 'u.name', 'u.image')->where('q.id',$id)
            ->leftJoin('answers AS a', 'a.question_id', '=', 'q.id')
            ->leftJoin('users AS u', 'u.id', '=', 'a.user_id')
            ->get();
    }
}
