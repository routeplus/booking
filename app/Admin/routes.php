<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('/users', UserController::class);
    $router->resource('/tasks', TasksController::class);
    $router->resource('/answers',AnswerController::class);
    $router->resource('/questions',QuestionController::class);
    $router->resource('/canvas',CanvasController::class);
     
    //ここは特にルーティングに関係なし
    


});
Route::get('image/{filename}', function ($filename)
{
    $path = storage_path() . '/app/image/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
}); 