@extends('layouts.mypage')

@section('content')
<div class="container">
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="profile-heading panel-heading">プロフィール<br>

        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th colspan="2">あなたの情報</th>
            </tr>
          </thead>
          <tr>
            <th>プロフィール写真</th>
            <td><img src="{{$user->image}}"></td>
          </tr>
          <tr>
            <th>お名前</th>
            <td>{{ $user->name }}</td>
          </tr>
          <tr>
            <th>メールアドレス</th>
            <td>{{ $user->email }}</td>
          </tr>
          <tr>
            <th>登録日</th>
            <td>{{ $user->created_at->diffForHumans() }}</td>

          </tr>
        </table>

        <div class="panel-body">
          <center><a href="/mypage/edit" class="profile-heading__edit"><button type="button" class="btn btn-outline-warning">プロフィールを編集</button></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
