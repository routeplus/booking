@extends('layouts.mypage')
@section('content')
  <div class="container">
    <div class="row">

      <form action="/mypage/commit/{{$user->id}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
        @endif


        <h3>プロフィールを編集</h3>

        <div class="form-group">
          <label for="name">お名前</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="新しい名前" value="{{ $user->name }}">
        </div>

        <div class="form-group">
          <label for="image">プロフィール画像</label>
          <input type="file" name="file" class="form-control">
        </div>

        <div class="form-group">
          <label for="email">メールアドレス</label>
          <input type="text" class="form-control" id="email" name="email" placeholder="新しいメールアドレス" value="{{ $user->email }}">
        </div>

        <div class="form-group">
          <label for="password">パスワード(変更する場合のみ)</label>
          <input type="password" class="form-control" id="password" name="password" placeholder="新しいパスワード">
        </div>

          <input type="submit" class="btn btn-primary">
          <a href="/mypage" class="profile-heading__edit"><button type="button" class="btn btn-default">戻る</button></a>
      </form>

    </div>
  </div>
@endsection
