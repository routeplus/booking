import styled from 'styled-components';
import { Media } from '../service/mediaHelper'

export const colors = {
  //Colors
  white: '#FFF',
  black: '#333',
  lightBlack: '#666',
  gray: '#999',
  ligthGray: '#CCC',
  blue: '#0a9b94',
  red: '#DC0011',
  secondary: '#efede0',
  pink: '#FD6476',
  success: '#08837d',
  warning: '#bf7600',
  text: '#3c4a60',
}


export const styles = {
  //color
  colorWhite: { color: '#FFF',},
  colorBlack: { color:'#333',},
  colorLightBlack: { color: '#666',},
  colorGray: { color:'#999',},
  colorLigthGray: { color:'#CCC',},
  colorBlue: {color: '#0a9b94',},
  colorRed: { color:'#DC0011',},
  colorPink: { color:'#FD6476',},
  colorSuccess: { color:'#08837d',},
  colorWarning: { color:'#bf7600',},
  colorText: { color:'#3c4a60',},

  //backgroundcolor
  bgWhite: { backgroundColor: '#FFF',},
  bgBlack: { backgroundColor:'#333',},
  bgLightBlack: { backgroundColor: '#666',},
  bgGray: { backgroundColor:'#999',},
  bgLigthGray: { backgroundColor:'#CCC',},
  bgBlue: {backgroundColor: '#0a9b94',},
  bgRed: { backgroundColor:'#DC0011',},
  bgPink: { backgroundColor:'#FD6476',},
  bgSuccess: { backgroundColor:'#08837d',},
  bgWarning: { backgroundColor:'#bf7600',},

  //font
  bold: {fontWeight: 'bold'},
  underline: {textDecoration: 'underline'},
  

  //margin
  mt0: { marginTop: 0},
  mt10: { marginTop: 10},
  mt20: { marginTop: 20},
  mt30: { marginTop: 30},
  mt40: { marginTop: 40},
  mt50: { marginTop: 50},
  mb0: { marginBottom: 0},
  mb5: { marginBottom: 5},
  mb10: { marginBottom: 10},
  mb15: { marginBottom: 15},
  mb20: { marginBottom: 20},
  mb25: { marginBottom: 25},
  mb30: { marginBottom: 30},
  mb35: { marginBottom: 35},
  mb40: { marginBottom: 40},
  mb45: { marginBottom: 45},
  mb50: { marginBottom: 50},
  mb55: { marginBottom: 55},
  mb60: { marginBottom: 60},
  ml1: { marginLeft: '1%'},
  ml2: { marginLeft: '2%'},
  ml3: { marginLeft: '3%'},
  ml4: { marginLeft: '4%'},
  ml5: { marginLeft: '5%'},
  mr1: { marginRight: '1%'},
  mr2: { marginRight: '2%'},
  mr3: { marginRight: '3%'},
  mr4: { marginRight: '4%'},
  mr5: { marginRight: '5%'},
  //padding
  pt0: { paddingTop: 0},
  pt5: { paddingTop: 5},
  pt10: { paddingTop: 10},
  pt20: { paddingTop: 20},
  pt30: { paddingTop: 30},
  pt40: { paddingTop: 40},
  pt50: { paddingTop: 50},
  pb0: { paddingBottom: 0},
  pb5: { paddingBottom: 5},
  pb10: { paddingBottom: 10},
  pb15: { paddingBottom: 15},
  pb20: { paddingBottom: 20},
  pb25: { paddingBottom: 25},
  pb30: { paddingBottom: 30},
  pb35: { paddingBottom: 35},
  pb40: { paddingBottom: 40},
  pb45: { paddingBottom: 45},
  pb50: { paddingBottom: 50},
  pb55: { paddingBottom: 55},
  pb60: { paddingBottom: 60},
  pl1: { paddingLeft: '1%'},
  pl2: { paddingLeft: '2%'},
  pl3: { paddingLeft: '3%'},
  pl4: { paddingLeft: '4%'},
  pl5: { paddingLeft: '5%'},
  pr1: { paddingRight: '1%'},
  pr2: { paddingRight: '2%'},
  pr3: { paddingRight: '3%'},
  pr4: { paddingRight: '4%'},
  pr5: { paddingRight: '5%'},

  //border-raduis
  br2: { borderRadius: '2px'},
  br5: { borderRadius: '5px'},
  br10: { borderRadius: '10px'},

}

export const mixStyles = function(){
  var res = {};
  for (var i = 0; i < arguments.length; ++i) {
    if (arguments[i]) Object.assign(res, arguments[i]);
  }
  return res;
}
