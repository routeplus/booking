//通信系はここに書く
import axios from 'axios'

let host = window.location.protocol + '//' + window.location.host

export const getDetail = id =>
  axios
    .get(host + '/api/v1/question/' + id + '/get', {})
    .then((results) => {
      const data = results.data
      console.log(data)
      return data
    })

export const getIndex = (limit=null, offset=null, orderBy=null) =>
  axios
    .get(host +'/api/v1/questions', {
      params: {
        // ここにクエリパラメータを指定する
        limit: limit,
        offset: offset,
        orderby: orderBy,
      }
    })
    .then((results) => {
      const data = results.data
      console.log(data)
      return data
    })

export const getQuestions = (limit=null, offset=null, orderBy=null) =>
  axios
    .get(host +'/api/v1/questions', {
      params: {
        // ここにクエリパラメータを指定する
        limit: limit,
        offset: offset,
        orderby: orderBy,
      }
    })
    .then((results) => {
      const data = results.data
      console.log(data)
      return data
    })

export const postQuestion = (question) =>
  axios
    .post(host +'/api/v1/question/commit',
    {
      "user_id":0,
      "mentor_id":0,
      "title": question.title,
      "body":  question.body
    })
    .then(function (response) {
    })
