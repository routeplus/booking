//通信系はここに書く
import axios from 'axios'
//let state = [app, like, field, feature]
let host = window.location.protocol + '//' + window.location.host

export const postCanvas = args =>
  axios
    .post(host +'/api/v1/canvas/commit', {'app': args.app,'like': args.like, 'field': args.field, 'feature': args.feature, 'user_id' : args.userId})
    .then((results) => {
      const data = results.data
      return data
    })

export const getCanvas = id =>
  axios
    .get(host + '/api/v1/canvas/' + id + '/get', {})
    .then((results) => {
      const data = results.data
      return data
    })
