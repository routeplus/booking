import { css, keyframes } from 'styled-components'

export const Media = {
  desktop: (...args) => css`
    @media (min-width: 1024px) {
      ${ css(...args) }
    }
  `,
  mobile: (...args) => css`
    @media (max-width: 1023px) {
      ${ css(...args) }
    }
  `,
  smallMobile: (...args) => css`
    @media (max-width: 350px) {
      ${ css(...args) }
    }
  `
}
