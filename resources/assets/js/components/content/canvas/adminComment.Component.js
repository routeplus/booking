import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { AdminComment } from './adminComment.styled'


export class AdminCommentComponent extends Component {
  render(props){
    return (
      <AdminComment>
        <p className="myStyle">{this.props.adminComment}</p>
        <p className="center">{this.props.adminComment}</p>
      </AdminComment>
    )
  }
}
