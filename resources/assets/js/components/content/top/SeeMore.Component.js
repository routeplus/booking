import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export class Content extends React.Component {
  constructor(props){
    super(props);
  }

  /*switchChange(e){
    const number = e;
    console.log(number);
    switch(number){
      case 1:
        return <div><p>{this.props.text1}</p>
                <p><a href={this.props.contentLink1}></a></p></div>
        break;

      case 2:
        return <div><p>{this.props.text2}</p>
                <p><a href={this.props.contentLink2}></a></p></div>
        break;
      case 3:
        return <div><p>{this.props.text3}</p>
                <p><a href={this.props.contentLink3}></a></p></div>
        break;
    }
  }*/

  render(){
    return(
      <div id="contents">
            <div className="content1">
          <p>{this.props.contentText1}</p>
          <p><a href={this.props.contentLink1}><img src={this.props.button_image} /></a></p>
      </div>
      <br />
      <div className="content2">
          <p>{this.props.contentText2}</p>
          <a href={this.props.contentLink2}><img src={this.props.button_image} /></a>
      </div>
      <br />
      <div className="content3">
          <p>{this.props.contentText3}</p>
          <a href={this.props.contentLink3}><img src={this.props.button_image} /></a>
      </div>

      </div>
    );
  }
}

if(document.getElementById('example')){
    ReactDOM.render(<Content />, document.getElementById('example'));
}
