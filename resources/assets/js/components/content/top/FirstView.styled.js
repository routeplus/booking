import styled from 'styled-components';
import { Media } from '../../../shared/service/mediaHelper'

export const FirstView = styled.div`
    margin: 0;
    height: 90vh;
    width: 100vw;
    background-image : url(../../images/kv.jpg);
    background-color : #00cc33;
    background-size: cover;

    #menbersButton{
        height: 20vh;
        width: 100vw;
        display: flex;
        justify-content: flex-end;
        text-align: center;
    }
    #RegisterLink{
        height: 4vh;
        width:  16vw;
        margin: auto 0.5vw;
        padding:2vh 1vw 1vh 1vw;
        border: 3px solid #fff;
        background-color: #00ff66;
        border-radius: 10px;
    }
    #loginLink{
        height: 4vh;
        width:  16vw;
        margin: auto 0.5vw;
        padding:2vh 1vw 1vh 1vw;
        border: 3px solid #fff;
        background-color: #ffcc00;
        border-radius: 10px;
    }
    a{
        text-decoration: none;
    }
    #mainText{
        font-size: 5vmin;
        margin: auto;
        text-align: center;
        color: #fff;
    }
    #Presentation{
        text-align: center;
        color: #fff;
        font-size: 4vmin;
        text-decoration: underline;
    }
    .bold{
        font-weight: bold;
    }
    #contents{
        width: 100vw;
        height: 20vh;
        margin: 0 auto 5vh auto;
        display: flex;
        justify-content: center;
        text-align: center;
    }
    .content1{
        width: 18vw;
        margin: 0 5vw 0 10vw;
        padding: 0 3vh 3vh 3vh;
        color: #fff;
        font-size: 1.5vw;
        font-weight: bold;
        background-image: url("../../images/istockphoto-843015650-2048x2048.jpg");
        background-size: cover;
        background-color: #00cc33;
    }
    .content2{
        width: 18vw;
        padding: 0 3vh 3vh 3vh;
        color: #fff;
        font-size: 1.5vw;
        font-weight: bold;
        background-image: url("../../images/istockp.jpg");
        background-size: cover;
        background-color: #00cc33;
    }
    .content3{
        width: 18vw;
        margin: 0 10vw 0 5vw;
        padding: 0 3vh 3vh 3vh;
        color: #fff;
        font-size: 1.5vw;
        font-weight: bold;
        background-image: url("../../images/layer31.png");
        background-size: cover;
        background-color: #00cc33;
    }
`;
