import styled from 'styled-components';
import { Media } from '../../../shared/service/mediaHelper'

export const Article = styled.div`
    width : 60vw;
    height : 100vh;
    main{
      width : 60vw;
      height : 90vh;
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      align-content:space-between;
    }
    .article-link{
      text-decoration: none;
    }
    .article{
      width: 29vw;
      height: 44vh;
      font-size: 1vmin;
      margin: 0;
      object-fit: cover;
      background-color: #eee
    }
    img.image{
      width: 29vw;
      height : auto;
      background-size: cover;
    }
    .text{
      margin: 0 1vmin 1vmin 1vmin;
    }
`;