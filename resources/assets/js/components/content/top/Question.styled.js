import styled from 'styled-components';
import { Media } from '../../../shared/service/mediaHelper'

export const Question = styled.div`
  padding: 1rem;
  margin-bottom: 20px;
  box-shadow:0px 0px 3px 2px #DDD;
  .question-link{
    cursor: pointer;
    color: #666;
    text-decoration: none;
  }
  .question-title{
    font-size: 1.6rem;
    border-bottom: 1px solid #CCC;
    padding-bottom: 0.8rem;
  }
  .title-heading{
    margin-right: 1rem;
  }
  .arrow{
    float: right;
  }
`;
