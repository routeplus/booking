import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styled from 'styled-components';
import { Media } from '../../../shared/service/mediaHelper'

export class HeadingComponent extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <Heading>
        <FontAwesomeIcon className='icon' icon={this.props.icon} color={this.props.color}/>
        {this.props.content}
      </Heading>
    );
  }
}

export const Heading = styled.h2`
  padding: 0.7rem 1rem;
  margin-bottom: 20px;
  box-shadow:0px 0px 3px 2px #DDD;
  .icon {margin-right: 10px;}
`;
