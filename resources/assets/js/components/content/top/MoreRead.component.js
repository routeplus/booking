import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export class MoreReadComponent extends React.Component {
	constructor() {
    super();
	};

  render(props){
    return (
    	<MoreRead href={this.props.uri} backgroundColor={this.props.backgroundColor} >
				{this.props.text}
    	</MoreRead>
    );
  }
}

const MoreRead = styled.a`
	color: #FFF;
	font-size: 1.6rem;
	display: block;
	width: 60%;
	margin: 0 20% 20px;
	padding: 1rem;
	text-align: center;
	text-decoration: none;
  background-color: ${(props) => {
    return props.backgroundColor
  }};
`;
