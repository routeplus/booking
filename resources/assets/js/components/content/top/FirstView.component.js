import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Content} from './SeeMore.Component'
import {FirstView}  from './FirstView.styled'

export class FirstviewComponent extends React.Component {
  constructor(props){
    super(props);
    //this.switchChange = this.switchChange.bind(this)
  }

  /*switchChange(e){
    const number = e
    const content = number =>{
        switch(number){
        case 1:
              return( <div key={number}>
                        <p>{this.props.text1}</p>
                        <p><a href={this.props.contentLink1}>詳しく読む</a></p>
                      </div>
              );
              break;

        case 2:
              return( <div key={number}>
                        <p>{this.props.text2}</p>
                        <p><a href={this.props.contentLink2}>詳しく読む</a></p>
                      </div>
              )
              break;
        case 3:
              return( <div key={number}>
                        <p>{this.props.text3}</p>
                        <p><a href={this.props.contentLink3}>詳しく読む</a></p>
                      </div>  
              )
              break;
      };
    }
    console.log({content})
    return <div key={number}>{content}what</div>
  }*/


  render(){
    return(
      <FirstView>
          <div id="mainWapper">
            <div id="menbersButton">
              <a href="/register" id="RegisterLink">会員登録</a>
              <a href='/login' id="loginLink">ログイン</a>
            </div>
            <div id="mainText">
              <p>プログラミング学習でもう迷わせない<br />
                <span className="bold">「作りながら学ぶ」</span>をサポートするサービス
              </p>
            </div>
            <div id="Presentation">
              <p><img src="../../images/logo.png" />ココカラエンジニアでできること</p>
            </div>
            <Content {...this.props} />
          </div>
      </FirstView>
    );
  }
}

if(document.getElementById('example')){
    ReactDOM.render(<FirstviewComponent/>, document.getElementById('example'));
}
