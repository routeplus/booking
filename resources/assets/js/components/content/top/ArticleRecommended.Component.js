import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import {Article} from './ArticleRecommended.styled';

export class Recommended extends React.Component {
	constructor() {
    super();
		
	};

	getArticle(props){
	    const article = props.map((article) =>{
	      return (
		    		<a href={this.props.articleLink1}>
		    			<img className="image" src={this.props.articleImageLink} />
		    			<div className="text">
		    				<h2>{this.props.articleTitle}</h2>
		    				<p>{this.props.articleText}</p>
		    			</div>
		    		</a>
	      );
	    });
	    return <div className="article">{article}</div>
	}

  render(){
    return (
    	<Article>
    		<main>
		    	<div className="article">
		    		<a href={this.props.articleLink1} className="article-link" target="_blank">
		    			<img className="image" src={this.props.articleImageLink1} />
		    			<div className="text">
		    				<h2>{this.props.articleTitle1}</h2>
		    				<p>{this.props.articleText1}</p>
		    			</div>
		    		</a>
		    	</div>
	    		<div className="article">
		    		<a href={this.props.articleLink2} className="article-link" target="_blank">
		    			<img className="image" src={this.props.articleImageLink2} />
		    			<div className="text">
		    				<h3>{this.props.articleTitle2}</h3>
		    				<p>{this.props.articleTtext2}</p>
		    			</div>
		    		</a>
		    	</div>
		    	<div className="article">
		    		<a href={this.props.articleLink3} className="article-link" target="_blank">
		    			<img className="image" src={this.props.articleImageLink3} />
		    			<div className="text">
		    				<h3>{this.props.articleTitle3}</h3>
		    				<p>{this.props.articleText3}</p>
		    			</div>
		    		</a>
		    	</div>
		    	<div className="article">
		    		<a href={this.props.articleLink4} className="article-link" target="_blank">
		    			<img className="image" src={this.props.articleImageLink4} />
		    			<div className="text">
		    				<h3>{this.props.articleTitle4}</h3>
		    				<p>{this.props.articleText4}</p>
		    			</div>
		    		</a>
		    	</div>
	    	</main>
	    </Article>
    );
  }
}
if (document.getElementById('example')) {
    ReactDOM.render(<Recommended />, document.getElementById('example'));
}