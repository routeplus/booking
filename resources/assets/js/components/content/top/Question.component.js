import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Question } from './Question.styled';

let questionList

export class QuestionComponent extends React.Component {
	constructor() {
    super();
	};

  qaList(){
		if(!this.props.questions)
		{
		  <h3>'aaa'</h3>
		}else{
	    const questionList = this.props.questions.map((question) => {
	      return (
				  <a className="question-link" href={`/question/${question.id}`}>
						<h3 className="question-title">
							<FontAwesomeIcon className="title-heading" icon='question' color='pink'/>
						  {question.title}
							<FontAwesomeIcon className="arrow" icon='arrow-circle-right' color='pink'/>
						</h3>
					</a>
	      );
	    });
			return questionList
	  }
	}

  render(props){
    return (
    	<Question>
				{this.qaList()}
    	</Question>
    );
  }
}
