import styled from 'styled-components';
import { Media } from '../../shared/service/mediaHelper'

export const List = styled.div`
  margin: 0;
  div{
    width: 70vw;
    height: 100px;
    background-color : #ccc;
    margin-bottom :10px;
    float:right;
    }
  div.image{
  	width: 100px;
  	height: 100px;
    float: left; 
  	background-color: #999;
  	margin-right: 2vw;
  }
  h2{
    background-color:#fff;
    height: 40px;
    padding:0;
    margin:0;
  }
  li{
 	  list-style: none;
    float:right;
  }
  p{
    color : #000;
    height:40px;
    margin:auto;
  	padding: 10px;
    background-color: #fff;
  }

  ${Media.mobile`
    h1{
      text-align: center;
    }
    ul{
      height: 60vh;
      padding: 0;
    }
    div{
      width: 100vw;
      height: 100px;
      padding-left: auto;
      padding-right: auto;
      margin: 0 3vw  10px 3vw;
    }
    div.image{
    }
  `}
`;