import styled from 'styled-components';

export const Sidebar = styled.aside`
      flex: 0.2;
      table,td,th{
        background-color:#fff;
        border: solid 1px #eeeeee;
        margin: 1vw auto auto auto;
        padding: 10px 20px;
        width: 20vw;
        height: 5vh;
        border-collapse: collapse;
      }
      a.podcastLink{
        text-decoration: none;
      }
      div.adSpace{
        width: 100%;
        height: auto;
        background-color: #ffffff;
        margin : 2vw auto;
      }
      div img{
        max-width: 100%;
        max-height: auto;
        background-size: cover;
        vertical-align: bottom;
      }
    `;
