import styled from 'styled-components';
import { Media } from '../../shared/service/mediaHelper'

export const Header = styled.header`
  display: flex;
  height: 15vh;
  background-color: #2FB872;
  padding: 10px;
  #headerLink1{
    margin-left: auto;
    justify-content: center;
    #headerImg1{
    padding: 2vh;
    height: 10vh;
    }
  }
  #headerLink2{
    vertical-align:middle
    height: 12vh;
    padding: 2vh;
    line-height: 12vh;
    margin-left: auto;
    font-size: 1.25rem;
    text-decoration:none;
    color: #ffffff;
  }
  #headerLink4{
    vertical-align:middle
    height: 12vh;
    padding: 2vh;
    line-height: 12vh;
    text-decoration:none;
    color: #ffffff;
  }
  #headerLink3{
    vertical-align:middle
    height: 15vh;
    line-height: 12vh;
    #headerImg2{
      height: 10vh;
      padding: 20px;
    }
  }



  ${Media.mobile`
    height: 8vh;
  `}
`;
