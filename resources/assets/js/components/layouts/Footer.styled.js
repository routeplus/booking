import styled from 'styled-components';

export const Footer = styled.footer`
  background-color: #5CB85C;
  display:flex;
  justify-content: space-around;
  padding: 1rem;
  ul{
      list-style-type: none;
  }
  .image-wrap{flex: 0.2}
  .image-wrap img{max-width: 100%;}
  .link a{ 
    color: #FFF;
    line-height: 1.5;
  }
  .link-list__left{
    flex: 0.3;
  } 
  .link-list__right{
    flex: 0.3;
  } 
`;
