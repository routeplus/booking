import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {Footer} from './Footer.styled';

export class FooterComponent extends React.Component {

  render(props) {
    return (
	    <Footer id="footer">
        <p className='image-wrap'><img id="headerImg1" src="/images/420logo.png" alt="ヘッダー画像1"/></p>
        <ul className="link-items__left">
          <li className="link"><a href="https://routeplus.co.jp" target='_blank'>運営会社</a></li>
          <li className="link"><a href="/about" target='_blank'>メンタボットとは？</a></li>
          <li className="link"><a href="/contact">お問い合わせ</a></li>
        </ul>
        <ul className="link-items__right">
          <li className="link"><a href='/register'>会員登録</a></li>
          <li className="link"><a href="https://se-shine.net" target='_blank'>初心者向けメディア「ココカラエンジニア」</a></li>
          <li className="link"><a href="https://se-shine.net/post_lp/lineat/" target='_blank'>Laravel基礎マスター講座（無料）</a></li>
        </ul>
      </Footer>
    );
  }
}
