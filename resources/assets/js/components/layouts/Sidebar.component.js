import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {Sidebar} from './Sidebar.styled';

export class SidebarComponent extends React.Component {

  render(){
    return (
      <Sidebar id="sidebar">
    
        <table>
          <tr>
            <td>MENU</td>
          </tr>
          <tr>
            <a className="podcastLink" href="/"><td>トップページ ></td></a>
          </tr>
          <tr>
            <a className="podcastLink" href="/mypage"><td>マイページ ></td></a>
          </tr>
          <tr>
            <a className="podcastLink" href="questions"><td>Q&A ></td></a>
          </tr>
          <tr>
            <a className="podcastLink" href="/ideanote"><td>アイデアノート ></td></a>
          </tr>
          <tr>
            <a className="podcastLink" href="/group"><td>コミュニティ ></td></a>
          </tr>
          <tr>
            <a className="podcastLink" href="https://se-shine.net/category/recruite/"><td>記事一覧 ></td></a>
          </tr>
          <tr>
            <a className="podcastLink" href="/practice"><td>課題 ></td></a>
          </tr>
          </table>
        <div className="adSpace"><a href="https://se-shine.net/startup-5article/"><img src="https://se-shine.net/wp-content/uploads/2018/01/567293f196bc64cd69820b79481e557c-1.jpg" /></a></div>
        <div className="adSpace"><a href="https://se-shine.net/beginner-to-programmer/"><img src="https://se-shine.net/wp-content/uploads/2018/02/7ebe9de0fb7d7a4cd521b57d8a82f587.jpg" /></a></div>
        <div className="adSpace"><a href="https://se-shine.net/post_lp/lineat/"><img src="https://se-shine.net/wp-content/uploads/2017/09/170e137c0db1e57cf67220e136407b7e-4.png" /></a></div>
      </Sidebar>
    );
  }
}

if(document.getElementById('example')){
    ReactDOM.render(<SidebarComponent />, document.getElementById('example'));
}
