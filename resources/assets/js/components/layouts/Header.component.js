import React, { Component } from 'react';
import ReactDOM from 'react-dom';


import {Header} from './Header.styled'

export class HeaderComponent extends React.Component {

  render(){
    return (
	    <Header id="header">
          <a id="headerLink1" className="" href="#"><img id="headerImg1" src="/images/420logo.png" alt="ヘッダー画像1"/></a>
          <a id="headerLink2" className="" href="#">打田裕馬</a>
          <a id="headerLink3" className="" href="#"><img id ="headerImg2" src="/images/IMG_1039-718x719.png" alt="ヘッダー画像2"/></a>
          <a id="headerLink4" className="" href="#">▼</a>
      </Header>
    );
  }
}
