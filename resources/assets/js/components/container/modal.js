import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import { postQuestion } from '../../shared/service/http/question';


export class ModalComponent extends React.Component {
	constructor() {
    super();
		this.state = {
			modalVisible : false,
			question:{title:"ここにタイトルを入れる",body:"ここに質問内容を入れる"}
		};
		this.openModal 	  = this.openModal.bind(this);
		this.closeModal   = this.closeModal.bind(this);
		this.handleChange = this.handleChange.bind(this);
	};

	openModal(){
		this.setState({modalVisible:true});
	}

	closeModal(){
		postQuestion(this.state.question);
		this.setState({modalVisible:false});
	}

	handleChange(event){
		const number = e.currentTarget.getAttribute('data-number')
		const input = event.target.value;
		switch(number){
			case 1:
				this.setState({title : input});
				break;

			case 2:
				this.setState({body : input});
				break;
		}
	}

  render(){
    return (
    	<div>
    		<textarea onClick={this.openModal} value={this.state.question.body}>モーダを開く</textarea>
	    	<ReactModal isOpen={this.state.modalVisible} animationType={'slide'} >
	    		<textarea className="qaTitle" value={this.state.question.title} onChange={this.handleChange} data-number="1">ここにタイトルを入力</textarea><br />
	    		<textarea className="qaBody" value={this.state.question.body} onChange={this.handleChange} data-number="2">ここに内容を入力</textarea><br />
	    		<button onClick={this.closeModal}>決定</button>
	    	</ReactModal>
	    </div>
    );
  }
}
if (document.getElementById('example')) {
    ReactDOM.render(<ModalComponent />, document.getElementById('example'));
}