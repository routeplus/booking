import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { styles, mixStyles } from '../../../shared/css/utility'

import {List} from './list.styled';
import { getQuestions } from '../../../shared/service/http/question';

let host = window.location.protocol + '//' + window.location.host

export class ListComponent extends React.Component {
  constructor(){
    super();
    this.state ={
      questions:[],
    }
  }

  //質問一覧を取ってくる
  componentDidMount(){
    getQuestions()
      .then((response) => {
        this.setState({
          questions: response
        })
      })
  }

  qaLIst(questions){
    const QAList = questions.map((list) =>{
      const url = host+'/question/'+list.id
      return (
        
          <div className="block_list" key={list.id} style={mixStyles(styles.pt5,styles.pr1,styles.pb5,styles.pl1)}>
          <a href={url} className="detailLink" style={mixStyles(styles.pt5,styles.pr3,styles.pb5,styles.pl3,styles.mb5)}>
            <li><h3>{list.title}</h3></li>
            <li>{list.body}</li>
            <li className="list_category" style={mixStyles(styles.colorWhite,styles.bgBlue,styles.br10,styles.mt10)}>{list.category_name}</li>
            </a>
          </div>
        
      )
    })
    return <ul>{QAList}</ul>
  }

  render(){
    return (
      <List>
        <h1 className="">Q&A一覧</h1>
        { this.qaLIst(this.state.questions) }
      </List>
    );
  }
}

if(document.getElementById('example')){
    ReactDOM.render(<ListComponent />, document.getElementById('example'));
}
