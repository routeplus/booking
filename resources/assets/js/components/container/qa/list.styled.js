import styled from 'styled-components';
import { Media } from '../../../shared/service/mediaHelper'

export const List = styled.div`
  width: 65vw;
  div{  
    margin: 0;
    width: 90%;
    float: left;
    background-color : #fff;
    margin-bottom :10px;
    height:100px;
  }
  h3{
    padding:0;
    margin:0;
  }
  li{
 	  list-style: none;
  }
  .block_list {
    position: relative;
    z-index: 0;
  }
  .block_list .detailLink {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    text-decoration: none;
    z-index: 1;
    background-color: #eee;
  }

  ${Media.mobile`
    h1{
      text-align: center;
    }
    ul{
      height: 60vh;
      padding: 0;
    }
    div{
      width: 100vw;
      height: 100px;
      padding-left: auto;
      padding-right: auto;
      margin: 0 3vw  10px 3vw;
    }
    div.image{
    }
    p{
      width:85vw;
      margin : auto 3vw;
    }
  `}
`;
