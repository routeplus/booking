import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import { postQuestion } from '../../../shared/service/http/question';


export class ModalComponent extends React.Component {
	constructor() {
    super()
		this.state = {
			modalVisible : false,
			question:{title: null}
		}
		this.openModal 		= this.openModal.bind(this)
		this.closeModal   = this.closeModal.bind(this)
		this.handleChange = this.handleChange.bind(this)
	}

	openModal(){
		this.setState({modalVisible:true})
	}

	closeModal(){
		console.log(this.state.title1,this.state.body2);
		postQuestion(this.state.question)
		this.setState({modalVisible:false})
	}

	handleChange(event){
		const input = event.target.value
    this.setState({title : input})
	}

  render(){
    return (
    	<div>
    		<textarea onClick={this.openModal} value={this.state.question.body}>質問を入力する・・・</textarea>
	    	<ReactModal isOpen={this.state.modalVisible} animationType={'slide'} >
	    		<textarea className="qaTitle" value={this.state.question.title} onChange={this.handleChange} placeholder="質問を入力してください・・・" data-number="1"></textarea>
      {/*<textarea className="qaBody" value={this.state.question.body} onChange={this.handleChange} data-number="2">ここに内容を入力</textarea><br /> */}
	    		<button onClick={this.closeModal}>決定</button>
	    	</ReactModal>
	    </div>
    );
  }
}
if (document.getElementById('example')) {
    ReactDOM.render(<ModalComponent />, document.getElementById('example'));
}
