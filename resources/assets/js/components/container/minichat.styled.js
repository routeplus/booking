import styled from 'styled-components';

export const Minichat = styled.div`
  position        : fixed;
  width           : 45vh;
  height          : 55vh;
  bottom          : -5px;
  z-index         : 100;
  right           : 30px;
  background-color: #FF0040;

  border　　　　　　:　solid;

  /*枠線*/
  padding       : 0;
  margin        : 0;
  font-weight   : bold;
  background    : #FFF;
  border        : solid 2px #808080;
  border-radius : 10px 10px 0px 0px;


  .title{
    display          : flex;
    justify-content  : center;
    margin           : 0px;
    font-weight      : lighter
    font-size        : 24px;
    background-color : #2FB872;
    color            : #ffffff;
    border-radius    : 8px 8px 0px 0px;
    cursor: pointer;
  }
  .closeButton{
    text-align: right;
    color : #ffffff;
    cursor: pointer;
    border : solid 2px #e2e2e2 ;
    background-color : #5ca4e7;
    padding : 0px 10px;
    border-radius: 50px;
    position: relative;
	　left: 10vh;
}


  /*チャットメッセージのバックグラウンド*/
  .chatMessageNewline{
    position  : relative;
    display   : inline-block;
    margin    : 1em 0 1em 15px;
    padding   : 7px 10px;
    min-width : 120px;
    max-width : 100%;
    color     : #f6f6f6;
    font-size : 16px;
    background: #4a8ae3;
    border-radius: 15px;
}
  }
  .chatMessageNewline:before {
    content     : "";
    position    : absolute;
    top         : 50%;
    left        : -30px;
    margin-top  : -15px;
    border      : 15px solid transparent;
    border-right: 15px solid #e0edff;
}
  .chatMessageNewline:after {
    content     : "";
    display     : inline-block;
    position    : absolute;
    top         : 18px;
    left        : -24px;
    border      : 12px solid transparent;
    border-right: 12px solid #4a8ae3;
}



  .chatErea{
    padding          : 20px;
    width            : auto;
    height           : 45vh;
    overflow         : scroll;
    background-color : #cbf2c3;
  }

  /*textarea*/
  .name{
    resize    : none;
    width     : 38vh;
    height    : 3vh;
    font-size : 16px;

  }
  .address{
    resize    : none;
    width     : 38vh;
    height    : 3vh;
    font-size : 16px;
  }
  .inputMessage{
    resize    : vertical;
    width     : 38vh;
    height    : 12vh;
    font-size : 16px;
  }
  #inputItem{
    margin : 0.2vh;
    fon

  }
  .inputRequired{
    font-size : 8px;
    color : #df3423;
  }

`;
