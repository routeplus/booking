import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {practice} from './PracticeList.styled'
import { styles, mixStyles } from '../../../shared/css/utility'

import { getPractice } from '../../../shared/service/http/practice'

export class PracticeListComponent extends React.Component {
  constructor(){
    super();
    this.state ={ 
      practics:[],
    }
  }
  async componentDidMount()
  {
    const getPracticeList = await getPractice()
    this.setState({
      practics : getPracticeList,
    })
  }

  PracticeList(practics){
    const practiceList = practics.map((list) =>{
      return (
        <practice key={list.id} className="">
          <img src={list.image} className="image" />
          <h3 className="practiceTitle">{list.title}</h3>
          <p className="practicebody">{list.body}</p>
        </practice>
      );
    });
    return <div className="practiceList">{practiceList}</div>
  }

  render(){
    return (
      <div>
        <h1>プログラミングの世界へようこそ！ ~ Hello World ~</h1>
        { this.PracticeList(this.state.practics) }
      </div>
    );
  }
}

if(document.getElementById('example')){
    ReactDOM.render(<PracticeListComponent />, document.getElementById('example'));
}