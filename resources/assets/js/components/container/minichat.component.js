import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import anime from 'animejs'

import { Minichat } from './minichat.styled';

export class MinichatComponent extends React.Component{
 closeChat() {
   var elem = document.getElementById('motion');
   elem.addEventListener('click',function(){
    anime({
      targets: motion,
      translateY: 380
    })
  })
}
openChat(){
  var elem = document.getElementById('motion');
  elem.addEventListener('click',function(){
   anime({
     targets: motion,
     translateY: 0
   })
 })
}

  constructor() {
    super();
    this.closeChat     = this.closeChat.bind(this);
    this.openChat      = this.openChat.bind(this);

  }


  render() {
    return (
      <Minichat id="motion">
        <div className="title">
          <a className="titleForced"onClick={this.openChat}>質問チャット</a>
          <a className="closeButton" onClick={this.closeChat}>×</a>
        </div>
        <div className="viewErea">
          <div className="chatErea">
            <div className="chatMessage">
              <p className="chatMessageNewline">ここにMessageここにMessageここにMessageここにMessage </p>
              <p className="chatMessageNewline">ここにMessageここにMessageここにMessageここにMessage </p>
              <p className="chatMessageNewline">ここにMessageここにMessageここにMessageここにMessage </p>
              <p className="chatMessageNewline">ここにMessageここにMessageここにMessageここにMessage </p>
            </div>
            <div className="inputErea">
              <div>
                <p id="inputItem"><a>お名前</a><a className="inputRequired">(必須)</a></p>
                <textarea className="name"></textarea>
                <p id="inputItem"><a>メールアドレス</a><a className="inputRequired">(必須)</a></p>
                <textarea className="address"></textarea>
                <p id="inputItem">質問内容</p>
                <textarea className="inputMessage"></textarea>
              </div>
            </div>
          </div>
        </div>
      </Minichat>
    );
  }
}
if (document.getElementById('example')) {
    ReactDOM.render(<MinichatComponent />, document.getElementById('example'));
}
