import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { getUser } from '../../shared/service/http/utility'
import { getCanvas, postCanvas } from '../../shared/service/http/canvas'
import { AdminCommentComponent } from '../../components/content/canvas/adminComment.Component'
import {SidebarComponent} from '../../components/layouts/Sidebar.component';
import {HeaderComponent}  from '../../components/layouts/Header.component';
import {FooterComponent}  from '../../components/layouts/Footer.component';
import { css, hover, media } from 'glamor';

import { CanvasStyle }  from './index.style';

const title = css({
  textAlign : 'center',
  fontSize : '30px',
  margin : '3%',
})

const center = css({
  textAlign : 'center',
})

const green = css({
  color : 'green',
})

const textarea = css ({
	width: '30vw',
	height: '15vw',
  resize : 'none',
})

const margin = css ({
  margin : '2vw',
})

const wrap = css ({
  display : 'flex',
  flexWrap : 'wrap',
})

const botton = css({
  backgroundColor : '#0FF',
  textAlign : 'center',
  marginBottom : '2rem',
})

export default class Canvas extends Component {
  constructor() {
    super()
    this.state = {
      like:null,
      app:null,
      feature:null,
      field:null,
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onTextAreaChange = this.onTextAreaChange.bind(this)
  }

  async componentDidMount(){
    const user = await getUser()
    let canvasData = await getCanvas(user.id)
    this.setState({
      adminComment: canvasData.message,
      like:canvasData.like,
      app:canvasData.app,
      feature:canvasData.feature,
      field:canvasData.field,
      userId:user.id,
    })
  }

  onSubmit(e){
    e.preventDefault()
    postCanvas(this.state)
  }

  onTextAreaChange(e){
    this.setState({ [e.target.name] : e.target.value })
  }

  render() {
    return (
      <CanvasStyle>
        <HeaderComponent />
        <div className='main-wrapper'>
          <main>
            <h1 {...title}>アプリ開発キャンバス</h1>
            <p {...center}>こちらの開発キャンバスの各項目に書き込むことでアプリ開発がより具体的になります<br></br>
             ３つ以上の項目を埋めると運営チームからコメントが付きます</p>
            <form onSubmit={this.onSubmit}>
              <div {...wrap}>
                <div {...margin}>
                  <p {...green}>・あなたの趣味や興味関心ごと</p>
                  <textarea {...textarea} name="like" onChange={this.onTextAreaChange} value={this.state.like}></textarea>
                </div>

                <div {...margin}>
                  <p {...green}>・好きなアプリ・Webサービス</p>
                  <textarea {...textarea} name="app" onChange={this.onTextAreaChange} value={this.state.app}></textarea>
                </div>

                <div {...margin}>
                  <p {...green}>・興味のある分野（アプリ・機械学習など）</p>
                  <textarea {...textarea} name="field" onChange={this.onTextAreaChange} value={this.state.field}></textarea>
                </div>

                <div {...margin}>
                  <p {...green}>・開発してみたいアプリ</p>
                  <textarea {...textarea} name="feature" onChange={this.onTextAreaChange} value={this.state.feature}></textarea>
                </div>

              </div>
              <div {...center}><input {...botton} type="submit" value="保存する"/></div>
            </form>
          </main>
          <SidebarComponent />
        </div>
        <FooterComponent />
      </CanvasStyle>
    )
  }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Canvas />, document.getElementById('example'))
}
