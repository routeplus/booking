import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HeaderComponent } from "../../components/layouts/Header.component";
import { FooterComponent } from "../../components/layouts/Footer.component";
import { PracticeListComponent } from "../../components/container/practice/PracticeList";


export default class Practice extends Component {
  constructor(props) {
      super(props);
      
    }

    render() {
      return (
        <practice className='main-wapper'>
          <HeaderComponent />
          <section>
            <main className='wapper'>
              <PracticeListComponent />
            </main>
          </section>
          <FooterComponent />
        </practice>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Practice />, document.getElementById('example'));
}
