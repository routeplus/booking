import styled from 'styled-components';
import { Media } from '../../shared/service/mediaHelper'

export const Index  = styled.main`
  .wapper{ display:flex; }
  .main{ margin: 5rem; }
  aside{ margin-top : 5rem; }
  .main-wrapper{ margin: 5vw; }
  .sidebar { 
  	flex: 0.2; 
  }

  ${Media.mobile`
    height: 8vh;
  `}
`;
