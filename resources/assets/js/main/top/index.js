import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../../../../../public/js/fontawesome.min.js'

import { styles, mixStyles } from '../../shared/css/utility'

import { getUser } from '../../shared/service/http/utility'
import { getQuestions } from '../../shared/service/http/question'

import { FirstviewComponent } from '../../components/content/top/FirstView.component';
import { Recommended } from '../../components/content/top/ArticleRecommended.Component';
import { QuestionComponent } from '../../components/content/top/Question.component';
import { MoreReadComponent } from '../../components/content/top/MoreRead.component';


import { HeadingComponent }  from '../../components/content/top/Heading.component';
import { FooterComponent }  from '../../components/layouts/Footer.component';
import { SidebarComponent } from '../../components/layouts/Sidebar.component';
import { Index }  from './index.styled';

class Top extends React.Component {
  constructor(){
    super();
    this.state = {
      questions : [],
    }
  }

  async componentDidMount()
  {
    const questions = await getQuestions(4,null,'DESC')
    this.setState({
      questions : questions,
    })
  }

  render(){
    return(
      <Index className='main-wapper'>
        <FirstviewComponent {...this.props} />
        <section className="wapper">
          <main className='main' style={mixStyles(styles.mt10, styles.mb10)} >
            <HeadingComponent color={'#FD6476'} icon={'question-circle'} content={'新着Q&A'}/>
            <QuestionComponent {...this.state} />
            <MoreReadComponent text={'すべてのQAを見る'} uri={'/questions'} backgroundColor={'#FD6476'}/>
            <HeadingComponent color={'#FD6476'} icon={'newspaper'} content={'オススメの記事'}/>
            <Recommended className='recommend' {...this.props}/>
          </main>
          <aside>
            <SidebarComponent className='sidebar'/>
          </aside>
        </section>
        <FooterComponent/>
      </Index>
    );
  }
}

let props = {
    //Q&A
    contentText1 : 'IT業界について質問できる',
    contentLink1 : '/question',
    //Canvas
    contentText2 : 'オリジナルサービスのアイデアを作れる',
    contentLink2 : '/canvas',
    //Task
    contentText3 : 'サイトの課題をクリアしてスキルアップ',
    contentLink3 : '/tasks',
    //button_image
    button_image : '../../images/btn_bg.png',
    //article_image
    article_image: '../../images/icn_article.png',
    //ArticleLink1
    articleLink1 : 'https://se-shine.net/interview-rock/',
    articleTitle1: '30代未経験からのエンジニア転職を成功させたROCKさんインタビュー',
    articleText1 : '今日はココカラエンジニア卒業生のROCKさんが自社開発をしている企業に転職が決まりましたので、カフェでインタビューさせていただきました。',
    articleImageLink1 : 'https://se-shine.net/wp-content/uploads/2018/10/success-rock-718x337.jpg',
    //ArticleLink2
    articleLink2 : 'https://se-shine.net/high-school-workers-programmer/',
    articleTitle2: '高卒フリーターからエンジニアになって人生逆転を狙え。年収600万以上のポテンシャルは誰にでもある',
    articleTtext2: '誰でも自由に働きながらたくさん稼ぎたいですよね？現在高卒フリーターに甘んじている方でもプログラマーになる事で、自由に働きながら600万円以上の年収を得られる可能性があります。',
    articleImageLink2 : 'https://se-shine.net/wp-content/uploads/2018/01/f8effde84bb756802b597be620fd5076-718x369.jpg',
    //ArticleLink3
    articleLink3 : 'https://se-shine.net/programmer-work/',
    articleTitle3: 'プログラマーの実際の仕事の内容、教えちゃいます',
    articleText3 : 'プログラミングを勉強してプログラマーになりたいという人が近年増えています。勉強しながら、学んだ言語をどんな風に仕事で使うのかな?と思うこともあるのではないでしょうか',
    articleImageLink3 : 'https://se-shine.net/wp-content/uploads/2018/10/cd260ab66496f405379f536807b156f7-718x391.png',
    //ArticleLink4
    articleLink4 : 'https://se-shine.net/beginner-laravel1/',
    articleTitle4: '【第1回】挫折させないLaravel php 環境構築　MAMPのインストール',
    articleText4 : 'こちらではPHPのウェブフレームワークlaravel（ララベル） とMAMP（マンプ）を使ってwebアプリを動かすための環境構築の説明をしていきます。',
    articleImageLink4 : 'https://se-shine.net/wp-content/uploads/2018/05/laravel1php-.jpg',
}


if(document.getElementById('example')){
    ReactDOM.render(<Top {...props}/>, document.getElementById('example'));
}
