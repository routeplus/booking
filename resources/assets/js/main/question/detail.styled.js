import styled from 'styled-components';
import { Media } from '../../shared/service/mediaHelper'

export const Detail  = styled.div`
  background-color:#eee;
  .questionDetail{
    border:solid 1px #999;
    background-color: #fff;
    border-radius: 5px;
  }
  .blockDetail{
    margin: 5vw;
  }
  .detailQuestionAnswerForm{
    height: 20vh;
    width: 70vw;
    background-color:#ddd;
    display: flex;
    position: relative;

  }
  .detailQuestionTextarea{
    height: 100%;
    width: 85%;
  }
  .detailQuestionSubmitButton{
    display: block;
    width: 15%;
    height: 20%;
    position: absolute;
    right: 0;
    bottom: 0;
  }
  .answerText{
    font:normal normal 70% "MS 明朝";
  }
  .respondent{
    font-style: "MS 明朝";
  }
`;