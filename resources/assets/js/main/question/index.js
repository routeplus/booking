import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { styles, mixStyles } from '../../shared/css/utility'


import { HeaderComponent }  from '../../components/layouts/Header.component';
import { FooterComponent }  from '../../components/layouts/Footer.component';
import { ListComponent }  from '../../components/container/qa/list';
import { SidebarComponent } from '../../components/layouts/Sidebar.component';
import { ModalComponent }  from '../../components/container/qa/modal.js';
import { MinichatComponent }from '../../components/container/minichat.component';
import { Index }  from './Index.styled';

 class QaList extends React.Component {
  constructor(){
    super();
  }

  render(){
    return(
      <Index className="wrapper">
        <MinichatComponent />
        <HeaderComponent />
        <div className="main-wrapper" style={mixStyles(styles.pt20,styles.pr5,styles.pb10,styles.pl5)}>
          <ListComponent className="list"/>
          <SidebarComponent className="sidebar" />
        </div>
        <FooterComponent />
      </Index>
    );
  }
}

if(document.getElementById('example')){
    ReactDOM.render(<QaList />, document.getElementById('example'));
}
