import styled from 'styled-components';
import { Media } from '../../shared/service/mediaHelper'

export const Index  = styled.main`
  .main-wrapper { 
    display: flex; 
  }
  .sidebar { flex: 0.2; }
  .list{
    width:65vw;
    margin-left:5vw;
    flex: 0.8;
  }
  .list_category{
  	width : 7vw;
  	text-align:center;
  }

  ${Media.mobile`
    height: 8vh;
  `}
`;
