import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { styles, mixStyles } from '../../shared/css/utility'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { getDetail } from '../../shared/service/http/question'
//import { getUser } from './shared/service/getContents'

import { HeaderComponent } from "../../components/layouts/Header.component"
import { FooterComponent } from "../../components/layouts/Footer.component"
import { Detail } from "./detail.styled"

const path = window.location.pathname
let a = path.split('/')
let id = a[2]
//URLを分解してパラメーターを取得

class QuestionDetail extends Component {
  constructor(){
    super()
    this.state = {
      question:[],
    }
  }

  async componentWillMount(){
     const question = await getDetail(id)
     this.setState({  question: question, })
  }

  question(question){
    const detail = question.map((detail) =>{
      console.log(detail)
      return (
        <div key={detail.id}>
          <div className="detail-question_title">
            <h3>{detail.title}</h3>
            <p className="detail_category" style={mixStyles(styles.colorWhite,styles.bgBlue,styles.br10,styles.mt10)}>{detail.category_name}</p>
          </div>
          <div className="respondent" style={mixStyles(styles.br10,styles.mt10)}>回答者情報</div>
          <div>
            <p className="detailQuestionAnswer answerText" style={mixStyles(styles.mt20,styles.mr5,styles.mb20,styles.ml5)}>{detail.answer_body}</p>
            <FontAwesomeIcon className="goodButton" icon='thumbs-up' color='lightblack'/>
            <p className="detailAnswerEvaluation answerText" style={mixStyles(styles.mt20,styles.mr5,styles.mb20,styles.ml5)}>高評価</p>
            <from className="detailQuestionAnswerForm" style={mixStyles(styles.pt20,styles.pl3,styles.pr5,styles.pb20,styles.br5)}>
              <textarea className="detailQuestionTextarea">この質問に回答する</textarea>
              <button type="submit" className="detailQuestionSubmitButton" style={mixStyles(styles.mt20,styles.mr2,styles.mb15,styles.ml2)}>投稿する</button>
            </from>
          </div>
        </div>
      )
    })
    return <div className="questionDetail" style={mixStyles(styles.pt10,styles.pl3,styles.pr5,styles.pb20)}>{detail}</div>
  }

  render() {
    return (
      <Detail className="App">
        <HeaderComponent />
        <h2 className="blockDetail">{this.question(this.state.question)}</h2>
        <FooterComponent />
      </Detail>
    )
  }
}
if (document.getElementById('root')) {
    ReactDOM.render(<QuestionDetail />, document.getElementById('root'));
}