<?php

use Illuminate\Database\Seeder;

class Admin_menuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->db->table('admin_menu')->insert([
        	'title' => 'Tasks',
        ]);
    }
}
